const sensor1 = {
	ref: 1,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor2 = {
	ref: 2,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor3 = {
	ref: 3,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor4 = {
	ref: 4,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor5 = {
	ref: 5,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor6 = {
	ref: 6,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor7 = {
	ref: 7,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor8 = {
	ref: 8,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor9 = {
	ref: 9,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor10 = {
	ref: 10,
	uuid: 'fe6f153b-b6f5-4c18-b30e-0934150b2249',
};
const sensor11 = {
	ref: 11,
	uuid: '293b0f25-be3c-488b-b93b-8e5b4ba4b9d2',
};
const sensor12 = {
	ref: 12,
	uuid: '293b0f25-be3c-488b-b93b-8e5b4ba4b9d2',
};
const sensor13 = {
	ref: 13,
	uuid: '293b0f25-be3c-488b-b93b-8e5b4ba4b9d2',
};
const sensor14 = {
	ref: 14,
	uuid: '293b0f25-be3c-488b-b93b-8e5b4ba4b9d2',
};
const sensor15 = {
	ref: 15,
	uuid: '293b0f25-be3c-488b-b93b-8e5b4ba4b9d2',
};
const sensor16 = {
	ref: 16,
	uuid: '293b0f25-be3c-488b-b93b-8e5b4ba4b9d2',
};
const sensor17 = {
	ref: 17,
	uuid: '293b0f25-be3c-488b-b93b-8e5b4ba4b9d2',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A10', device: device };

module.exports.dryer = dryer;
