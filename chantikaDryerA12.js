const sensor1 = {
	ref: 1,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor2 = {
	ref: 2,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor3 = {
	ref: 3,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor4 = {
	ref: 4,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor5 = {
	ref: 5,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor6 = {
	ref: 6,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor7 = {
	ref: 7,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor8 = {
	ref: 8,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor9 = {
	ref: 9,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor10 = {
	ref: 10,
	uuid: '79f1fa42-ed76-4330-a026-bebf9171de78',
};
const sensor11 = {
	ref: 11,
	uuid: 'fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6',
};
const sensor12 = {
	ref: 12,
	uuid: 'fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6',
};
const sensor13 = {
	ref: 13,
	uuid: 'fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6',
};
const sensor14 = {
	ref: 14,
	uuid: 'fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6',
};
const sensor15 = {
	ref: 15,
	uuid: 'fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6',
};
const sensor16 = {
	ref: 16,
	uuid: 'fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6',
};
const sensor17 = {
	ref: 17,
	uuid: 'fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A12', device: device };

module.exports.dryer = dryer;
