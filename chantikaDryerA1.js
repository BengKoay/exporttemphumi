const sensor1 = {
	ref: 1,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor2 = {
	ref: 2,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor3 = {
	ref: 3,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor4 = {
	ref: 4,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor5 = {
	ref: 5,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor6 = {
	ref: 6,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor7 = {
	ref: 7,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor8 = {
	ref: 8,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor9 = {
	ref: 9,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor10 = {
	ref: 10,
	uuid: 'b5153d6f-8bda-4004-a7b0-9e403cf5f333',
};
const sensor11 = {
	ref: 11,
	uuid: '107e369a-9f82-43c1-ad12-9b91bdee917a',
};
const sensor12 = {
	ref: 12,
	uuid: '107e369a-9f82-43c1-ad12-9b91bdee917a',
};
const sensor13 = {
	ref: 13,
	uuid: '107e369a-9f82-43c1-ad12-9b91bdee917a',
};
const sensor14 = {
	ref: 14,
	uuid: '107e369a-9f82-43c1-ad12-9b91bdee917a',
};
const sensor15 = {
	ref: 15,
	uuid: '107e369a-9f82-43c1-ad12-9b91bdee917a',
};
const sensor16 = {
	ref: 16,
	uuid: '107e369a-9f82-43c1-ad12-9b91bdee917a',
};
const sensor17 = {
	ref: 17,
	uuid: '107e369a-9f82-43c1-ad12-9b91bdee917a',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A1', device: device };

module.exports.dryer = dryer;
