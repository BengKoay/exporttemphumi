const sensor1 = {
	ref: 1,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor2 = {
	ref: 2,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor3 = {
	ref: 3,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor4 = {
	ref: 4,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor5 = {
	ref: 5,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor6 = {
	ref: 6,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor7 = {
	ref: 7,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor8 = {
	ref: 8,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor9 = {
	ref: 9,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor10 = {
	ref: 10,
	uuid: '0c141b4b-cf35-4f01-aa52-c92364529afd',
};
const sensor11 = {
	ref: 11,
	uuid: '035342a4-5414-4ae0-998b-99dd907b070b',
};
const sensor12 = {
	ref: 12,
	uuid: '035342a4-5414-4ae0-998b-99dd907b070b',
};
const sensor13 = {
	ref: 13,
	uuid: '035342a4-5414-4ae0-998b-99dd907b070b',
};
const sensor14 = {
	ref: 14,
	uuid: '035342a4-5414-4ae0-998b-99dd907b070b',
};
const sensor15 = {
	ref: 15,
	uuid: '035342a4-5414-4ae0-998b-99dd907b070b',
};
const sensor16 = {
	ref: 16,
	uuid: '035342a4-5414-4ae0-998b-99dd907b070b',
};
const sensor17 = {
	ref: 17,
	uuid: '035342a4-5414-4ae0-998b-99dd907b070b',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A7', device: device };

module.exports.dryer = dryer;
