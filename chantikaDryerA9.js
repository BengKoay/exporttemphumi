const sensor1 = {
	ref: 1,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor2 = {
	ref: 2,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor3 = {
	ref: 3,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor4 = {
	ref: 4,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor5 = {
	ref: 5,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor6 = {
	ref: 6,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor7 = {
	ref: 7,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor8 = {
	ref: 8,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor9 = {
	ref: 9,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor10 = {
	ref: 10,
	uuid: '0ffae8d7-6242-4585-90f1-73b03d9345f4',
};
const sensor11 = {
	ref: 11,
	uuid: '471f067b-aff6-40ed-9629-32a19560c16e',
};
const sensor12 = {
	ref: 12,
	uuid: '471f067b-aff6-40ed-9629-32a19560c16e',
};
const sensor13 = {
	ref: 13,
	uuid: '471f067b-aff6-40ed-9629-32a19560c16e',
};
const sensor14 = {
	ref: 14,
	uuid: '471f067b-aff6-40ed-9629-32a19560c16e',
};
const sensor15 = {
	ref: 15,
	uuid: '471f067b-aff6-40ed-9629-32a19560c16e',
};
const sensor16 = {
	ref: 16,
	uuid: '471f067b-aff6-40ed-9629-32a19560c16e',
};
const sensor17 = {
	ref: 17,
	uuid: '471f067b-aff6-40ed-9629-32a19560c16e',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A9', device: device };

module.exports.dryer = dryer;
