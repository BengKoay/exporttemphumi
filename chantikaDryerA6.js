const sensor1 = {
	ref: 1,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor2 = {
	ref: 2,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor3 = {
	ref: 3,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor4 = {
	ref: 4,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor5 = {
	ref: 5,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor6 = {
	ref: 6,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor7 = {
	ref: 7,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor8 = {
	ref: 8,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor9 = {
	ref: 9,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor10 = {
	ref: 10,
	uuid: '81ae2ff4-8f6b-4c26-b88b-24163c422917',
};
const sensor11 = {
	ref: 11,
	uuid: '01ab3267-b70c-49d3-a7aa-e7c22f044dee',
};
const sensor12 = {
	ref: 12,
	uuid: '01ab3267-b70c-49d3-a7aa-e7c22f044dee',
};
const sensor13 = {
	ref: 13,
	uuid: '01ab3267-b70c-49d3-a7aa-e7c22f044dee',
};
const sensor14 = {
	ref: 14,
	uuid: '01ab3267-b70c-49d3-a7aa-e7c22f044dee',
};
const sensor15 = {
	ref: 15,
	uuid: '01ab3267-b70c-49d3-a7aa-e7c22f044dee',
};
const sensor16 = {
	ref: 16,
	uuid: '01ab3267-b70c-49d3-a7aa-e7c22f044dee',
};
const sensor17 = {
	ref: 17,
	uuid: '01ab3267-b70c-49d3-a7aa-e7c22f044dee',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A6', device: device };

module.exports.dryer = dryer;
