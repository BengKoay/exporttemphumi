const sensor1 = {
	ref: 1,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor2 = {
	ref: 2,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor3 = {
	ref: 3,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor4 = {
	ref: 4,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor5 = {
	ref: 5,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor6 = {
	ref: 6,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor7 = {
	ref: 7,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor8 = {
	ref: 8,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor9 = {
	ref: 9,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor10 = {
	ref: 10,
	uuid: '2993dc00-1f84-48a6-b30d-1ce4f3f43445',
};
const sensor11 = {
	ref: 11,
	uuid: 'c17ac968-106b-493a-b1e9-6796a7a4a0e8',
};
const sensor12 = {
	ref: 12,
	uuid: 'c17ac968-106b-493a-b1e9-6796a7a4a0e8',
};
const sensor13 = {
	ref: 13,
	uuid: 'c17ac968-106b-493a-b1e9-6796a7a4a0e8',
};
const sensor14 = {
	ref: 14,
	uuid: 'c17ac968-106b-493a-b1e9-6796a7a4a0e8',
};
const sensor15 = {
	ref: 15,
	uuid: 'c17ac968-106b-493a-b1e9-6796a7a4a0e8',
};
const sensor16 = {
	ref: 16,
	uuid: 'c17ac968-106b-493a-b1e9-6796a7a4a0e8',
};
const sensor17 = {
	ref: 17,
	uuid: 'c17ac968-106b-493a-b1e9-6796a7a4a0e8',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A3', device: device };

module.exports.dryer = dryer;
