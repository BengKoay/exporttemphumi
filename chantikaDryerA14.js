const sensor1 = {
	ref: 1,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor2 = {
	ref: 2,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor3 = {
	ref: 3,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor4 = {
	ref: 4,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor5 = {
	ref: 5,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor6 = {
	ref: 6,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor7 = {
	ref: 7,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor8 = {
	ref: 8,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor9 = {
	ref: 9,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor10 = {
	ref: 10,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
};
const sensor11 = {
	ref: 11,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
	// uuid: 'e722e20e-1b44-4fd4-bd35-57c425d91ce7',
};
const sensor12 = {
	ref: 12,
	uuid: 'fd2de1c6-3f89-4965-af03-fed5959ca716',
	// uuid: 'e722e20e-1b44-4fd4-bd35-57c425d91ce7',
};
const sensor13 = {
	ref: 13,
	uuid: 'e722e20e-1b44-4fd4-bd35-57c425d91ce7',
};
const sensor14 = {
	ref: 14,
	uuid: 'e722e20e-1b44-4fd4-bd35-57c425d91ce7',
};
const sensor15 = {
	ref: 15,
	uuid: 'e722e20e-1b44-4fd4-bd35-57c425d91ce7',
};
const sensor16 = {
	ref: 16,
	uuid: 'e722e20e-1b44-4fd4-bd35-57c425d91ce7',
};
const sensor17 = {
	ref: 17,
	uuid: 'e722e20e-1b44-4fd4-bd35-57c425d91ce7',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A14', device: device };

module.exports.dryer = dryer;
