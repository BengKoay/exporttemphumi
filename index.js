const axios = require('axios');
const moment = require('moment');
const ObjectsToCsv = require('objects-to-csv');

const XLSX = require('xlsx');

const axiosSolar = {
	baseURL: 'https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/',
	authorization: 'a414f268-5ab2-4854-b4f7-97a9ea6d6fec',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { Authorization: axiosSolar.authorization },
});

// const deviceId = 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a';
// const ref = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// const dryer = require('./chantikaDryerA1').dryer;
// const dryer = require('./chantikaDryerA2').dryer;
// const dryer = require('./chantikaDryerA3').dryer;
// const dryer = require('./chantikaDryerA4').dryer;
// const dryer = require('./chantikaDryerA5').dryer;
// const dryer = require('./chantikaDryerA6').dryer;
// const dryer = require('./chantikaDryerA7').dryer;
// const dryer = require('./chantikaDryerA8').dryer;
// const dryer = require('./chantikaDryerA9').dryer;
// const dryer = require('./chantikaDryerA10').dryer;
// const dryer = require('./chantikaDryerA11').dryer;
// const dryer = require('./chantikaDryerA12').dryer;
const dryer = require('./chantikaDryerA13').dryer;
// const dryer = require('./chantikaDryerA14').dryer;

const device = dryer.device;
const name = dryer.name;

// const timestampFrom = moment('2022-03-30 18:00', 'YYYY-MM-DD HH:mm').unix(); //`${moment().startOf('day').subtract(1, 'days').unix()}`; // start of yesterday
// const timestampTo = moment('2022-03-31 08:00', 'YYYY-MM-DD HH:mm').unix();

const timestampFrom = `${moment().subtract(2, 'h').unix()}`;
// // // const timestampFrom = `${moment().startOf('day').subtract(1, 'days').unix()}`; // start of yesterday
const timestampTo = moment().unix(); // now

const pushRefToDataPoint = [];

const main = async () => {
	// const startDate = moment.utc().subtract(1, 'day').unix(); // moment().startOf('day').unix() * 1000

	const startDate = timestampFrom; // moment().startOf('day').unix() * 1000
	const dateNow = timestampTo;

	// const startDate = 1640019969; // moment().startOf('day').unix() * 1000
	// const dateNow = 1640079000;
	// const dateNow = moment().unix();
	// const dateNow = 1639972906;
	// const data = {};

	// get data
	const data = await getData(startDate, dateNow);

	// await writeToExcel(data, startDate, dateNow);

	const infoTemperature = await convertDataToCsvFormat(
		data,
		startDate,
		dateNow
	);

	// write to csv
	const csv = new ObjectsToCsv(infoTemperature);
	await csv.toDisk(`./${name}_${startDate}_${dateNow}.csv`);
};

const getData = async (startDate, dateNow) => {
	// console.log('testOne');
	// console.log(startDate);
	// console.log(dateNow);
	const collectedData = [];
	for (const eachDevice of device) {
		const data = {};
		// console.log('eachDevice', eachDevice);
		data.ref = eachDevice.ref;
		data.data = [];
		try {
			const res = await axiosMainHelper.get(
				// `devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
				`devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
			);
			// console.log(res);
			for (const row of res.data.data) {
				const info = {};
				info.ref = eachDevice.ref;
				// console.log(row);
				// console.log(row.deviceId);
				// console.log(row.ref, row.temperature, row.humidity);
				if (row.hasOwnProperty('timestamp')) {
					info.timestamp = moment(row.timestamp).format('YYYY-MM-DD HH:mm:ss');
				}
				if (row.hasOwnProperty('temperature')) {
					info.temperature = row.temperature;
				}
				if (row.hasOwnProperty('humidity')) {
					info.humidity = row.humidity;
				}
				if (row.hasOwnProperty('deviceId')) {
					info.deviceId = row.deviceId;
				}
				if (row.hasOwnProperty('ref')) {
					info.ref = row.ref;
				}
				// console.log(row.temperature);
				// console.log(row.humidity);
				data.data.push(info);
			}
		} catch (e) {
			console.log('error ', e);
		}
		// console.log('data.ref', data.ref, data.data);
		collectedData.push(data);
		// console.log(collectedData);
	}
	// console.log('data', data);
	return collectedData;
};

const writeToExcel = async (data, startDate, dateNow) => {
	let wb = XLSX.utils.book_new();
	let exportFileName = `${name}_${startDate}_${dateNow}.xlsx`;
	for (const eachRef of data) {
		// console.log('writeToExcel', eachRef);
		// for each array loop, eachRef
		let ws = XLSX.utils.json_to_sheet(eachRef.data);
		ws['!cols'] = [
			{ wch: 20 },
			{ wch: 20 },
			{ wch: 20 },
			{ wch: 20 },
			{ wch: 20 },
			{ wch: 20 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
		];
		let sheetName = `${eachRef.ref}`;
		console.log('sheetName', sheetName);
		XLSX.utils.book_append_sheet(wb, ws, sheetName);
	}
	XLSX.writeFile(wb, exportFileName);
};
const convertDataToCsvFormat = async (data, startDate, dateNow) => {
	console.log('testTwo');
	// let startDateMoment = moment().unix(startDate);
	// let dateNowMoment = moment().unix(dateNow);
	let startDateMoment = moment(moment.unix(startDate)).unix();
	let dateNowMoment = moment(moment.unix(dateNow)).unix();
	// console.log('data', data);
	console.log(startDateMoment);
	console.log(dateNowMoment);
	const infoTemperature = [];
	// const infoHumidity = [];
	//process data in evey 5 minutes
	try {
		//process data in evey 5 minutes
		let startMinutes = moment(startDate).get('minutes');
		let endMinutes = moment(dateNow).get('minutes');

		let startProcess = moment(startDate * 1000).subtract(
			startMinutes,
			'minutes'
		);
		let endProcess = moment(dateNow * 1000).add(endMinutes, 'minutes');
		let tempDiff = 0;
		console.log(
			'startMinutes',
			typeof startDate,
			startDate,
			startMinutes,
			startProcess.format('YYYY-MM-DD HH:mm'),
			startProcess.unix()
		);
		console.log(
			'endMinutes',
			typeof dateNow,
			dateNow,
			endMinutes,
			endProcess.format('YYYY-MM-DD HH:mm'),
			endProcess.unix()
		);
		while (startProcess.unix() < endProcess.unix()) {
			let nextProcess = moment(startProcess).add(1, 'minutes');
			const dataTemperature = {
				time: startProcess.format('YYYY-MM-DD HH:mm'),
				'1_temperature': 0,
				'1_humidity': 0,
				'2_temperature': 0,
				'2_humidity': 0,
				'3_temperature': 0,
				'3_humidity': 0,
				'4_temperature': 0,
				'4_humidity': 0,
				'5_temperature': 0,
				'5_humidity': 0,
				'6_temperature': 0,
				'6_humidity': 0,
				'7_temperature': 0,
				'7_humidity': 0,
				'8_temperature': 0,
				'8_humidity': 0,
				'9_temperature': 0,
				'9_humidity': 0,
				'10_temperature': 0,
				'10_humidity': 0,
				'11_temperature': 0,
				'11_humidity': 0,
				'12_temperature': 0,
				'12_humidity': 0,
				'13_temperature': 0,
				'13_humidity': 0,
				'14_temperature': 0,
				'14_humidity': 0,
				'15_temperature': 0,
				'15_humidity': 0,
				'16_temperature': 0,
				'16_humidity': 0,
				'17_temperature': 0,
				'17_humidity': 0,
			};
			for (const eachData of data) {
				for (const dataPoint of eachData.data) {
					if (
						moment(dataPoint.timestamp).unix() >= startProcess.unix() &&
						moment(dataPoint.timestamp).unix() < nextProcess.unix()
					) {
						if (dataPoint.hasOwnProperty('temperature')) {
							dataTemperature[`${dataPoint.ref}_temperature`] =
								dataPoint.temperature;
						}
						if (dataPoint.hasOwnProperty('temperature')) {
							dataTemperature[`${dataPoint.ref}_humidity`] = dataPoint.humidity;
						}
					}
				}
			}
			infoTemperature.push(dataTemperature);
			startProcess = nextProcess;
		}
	} catch (e) {
		console.log('error', e);
	}

	// console.log('infoTemperature', infoTemperature);
	return infoTemperature;
};

main();
