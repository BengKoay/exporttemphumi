const sensor1 = {
	ref: 1,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor2 = {
	ref: 2,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor3 = {
	ref: 3,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor4 = {
	ref: 4,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor5 = {
	ref: 5,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor6 = {
	ref: 6,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor7 = {
	ref: 7,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor8 = {
	ref: 8,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor9 = {
	ref: 9,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor10 = {
	ref: 10,
	uuid: '116e9fe0-6e05-4e49-a18e-191834dbc56c',
};
const sensor11 = {
	ref: 11,
	uuid: 'f750df7e-15a6-4244-868e-843454a52a61',
};
const sensor12 = {
	ref: 12,
	uuid: 'f750df7e-15a6-4244-868e-843454a52a61',
};
const sensor13 = {
	ref: 13,
	uuid: 'f750df7e-15a6-4244-868e-843454a52a61',
};
const sensor14 = {
	ref: 14,
	uuid: 'f750df7e-15a6-4244-868e-843454a52a61',
};
const sensor15 = {
	ref: 15,
	uuid: 'f750df7e-15a6-4244-868e-843454a52a61',
};
const sensor16 = {
	ref: 16,
	uuid: 'f750df7e-15a6-4244-868e-843454a52a61',
};
const sensor17 = {
	ref: 17,
	uuid: 'f750df7e-15a6-4244-868e-843454a52a61',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

const dryer = { name: 'Chantika Dryer A4', device: device };

module.exports.dryer = dryer;
